Pwned
=====

Command line interface for checking if your online accounts have been
compromised via `haveibeenpwned.com <https://haveibeenpwned.com/>`__.

Installation
------------

Via PyPI:

.. code:: bash

  pip install pwned

From source:

.. code:: bash

  git clone https://github.com/SShrike/pwned.git
  cd pwned
  python setup.py bdist_wheel
  pip install dist/*
