# pwned - CLI for checking if your online accounts are compromised.
# Copyright (c) 2016 Severen Redwood <severen@shrike.me>
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

__version__ = '0.1.2'
